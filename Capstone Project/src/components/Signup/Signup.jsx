import React, { useEffect,useState } from 'react'
import { Link,  useNavigate } from 'react-router-dom'
import {api_uri} from '../../config';
import "./Signup.css"

const Signup = () => {
    const[name,setName]=useState();
    const[email,setEmail]=useState();
    const[phone,setPhone]=useState();
    const[password,setPassword]=useState();
    const [showerr,setShowere]=useState();
    const [passError,setPassError]=useState();
    const navigate=useNavigate();   
    useEffect (() => {
        const styleobject= document.getElementById('navbar_container');
        styleobject.style.display='block';
    })
    const Register=async(e)=>{
     e.preventDefault();
     //api call
     if (password.length<=8) {
        const message='Password should be of minimum 8 characters';
        setPassError(message)
     }
     else{
     const response= await fetch(`${api_uri}/api/auth/register`,{
     method:'POST',
     headers:{
        'content-type':'application/json'
     },
     body:JSON.stringify({
        name:name,
        phone:phone,
        password:password,
        email:email,
     })
    })
    const json=await response.json();
    //console.log('json data',json);

    if (json.authtoken) {
        localStorage.setItem('auth-token',json.authtoken);
        localStorage.setItem('name',name);
        navigate('/customer');
        window.location.reload();
    }
    if (json.error) {
        //showerr=json.error;
        setShowere(json.error);
        //console.log();

    }
}
    }
  return (
    <>
    <div className='signup_container'>
        <div className="signup_main">
            <div className="signupgrid">
                <h1>Signup</h1>
            </div>
            <div className="signup_text">
                Already a member <Link to='/login'>LogIn</Link>
            </div>
            <div className="signup_form">
                <form onSubmit={Register}>
                    {/* name */}
                    <div className="mb-3">
                        <label htmlFor='name' className='form-label'>Enter Name</label>
                        <input type='text' id='name' className='form-control' placeholder='Enter Name' value={name} onChange={(e) =>setName(e.target.value)}/>
                    </div>

                    {/* Email */}
                    <div className="mb-3">
                        <label htmlFor='email' className='form-label'>Enter Email</label>
                        <input type='text' id='email' className='form-control' placeholder='Enter Email' value={email} onChange={(e) =>setEmail(e.target.value)}/>
                        {showerr && <div style={{color:'blue',fontSize:'20px',fontWeight:'bold'}} >{showerr}</div>}
                        {/* Phone */}
                        </div>
                    <div className="mb-3">
                        <label htmlFor='phone' className='form-label'>Enter Phone Nmber</label>
                        <input type='text' id='phone' className='form-control' placeholder='Enter Phone number' value={phone} onChange={(e) =>setPhone(e.target.value)}/>
                        </div>
                        {/* Password */}
                    <div className="mb-3">
                        <label htmlFor='password' className='form-label'>Enter Password</label>
                        <input type='password' id='password' className='form-control' placeholder='Enter Password' value={password} onChange={(e) =>setPassword(e.target.value)}/>
                        {passError && <div style={{color:'blue',fontWeight:'bold',fontSize:'20px'}} >{passError}</div>}
                    </div>
                    <button type='submit' className='btn_signp'>Submit</button>
                </form>
            </div>
        </div>
    </div>
    </>
  )
}

export default Signup