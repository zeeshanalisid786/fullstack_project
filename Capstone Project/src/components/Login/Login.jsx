import React, {  useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {api_uri} from '../../config';
import './Login.css'
const Login = () => {
    const [email, setEmail]= useState();
    const [password, setPassword]= useState();
    const navigate=useNavigate();

    

    const loginuser =async (e) => {
        e.preventDefault();
       // console.log('loginuser');
        const authtoken=localStorage.getItem('auth-token');

        const response=await fetch(`${api_uri}/api/auth/login`,{
            method:'POST',
            headers:{
                'content-type':'application/json',
               // 'Authoriztion':`Bearer ${token}`,
            },
            body:JSON.stringify({
                email:email,
                password:password
            })
        })
        const json=await response.json();
        console.log('json data',json);
    
        if (json.authtoken) {
            localStorage.setItem('auth-token',json.authtoken);//catch response from auth.jsx
            localStorage.setItem('name',json.userName);//catch response from auth.jsx
            navigate('/customer');
            window.location.reload();
        }
        if (json.error) {
            //showerr=json.error;
            setShowere(json.error);
            //console.log();
    
        }
    }
  return (
    <>
    <div className='signup_container'>
        <div className="signup_main">
            <div className="signupgrid">
                <h1>Login</h1>
            </div>
            <div className="signup_text">
                New to FooGo! <Link to='/signup'>SinUp</Link>
            </div>
            <div className="signup_form">
                <form onSubmit={loginuser}>
                    {/* Email */}
                    <div className="mb-3">
                        <label htmlFor='email' className='form-label'>Enter Email</label>
                        <input type='text' id='email' className='form-control' placeholder='Enter Email' value={email} onChange={(e) => setEmail(e.target.value)}/>
                       </div>
                        {/* Password */}
                    <div className="mb-3">
                        <label htmlFor='password' className='form-label'>Enter Password</label>
                        <input type='password' id='password' className='form-control' placeholder='Enter Password' value={password} onChange={(e) =>setPassword(e.target.value)}/>
                    </div>
                    <button type='submit' className='btn_login'>LogIn</button>
                </form>
            </div>
        </div>
    </div>
    </>
  )
}

export default Login