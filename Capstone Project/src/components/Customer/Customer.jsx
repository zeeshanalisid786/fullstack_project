import React, { useEffect, useState } from 'react'
import {  useNavigate } from 'react-router-dom'
import './Customer.css';
import {api_uri} from '../../config';
export default function Customer(){
  
  const [selectedItemId, setSelectedItemId] = useState('');
  const [selectedItem, setSelectedItem] = useState(null);
  const [foodItems, setFoodItems] = useState([]);
   const [confirmationDetails,setconfirmationDetails]=useState({
     reservationUserName:null
    // userPhone:'null',
   });
   const available=(true);
   const [selectedPlace,setSelectedPlace]=useState(null);

   
   const [reservationConfirmed, setReservationConfirmed]=useState(false);

   useEffect(() => {
    // Fetch food items from backend when component mounts
    fetch(`${api_uri}/api/foodItems/handleAddFoodItem`)
      .then(response => response.json())
      .then(data => setFoodItems(data))
      .catch(error => console.error('Error fetching food items:', error));
  }, []);

   const handelReservation=(id)=>{
    //console.log(id);
    setSelectedItemId(id);
    
    
    setSelectedPlace(available);
    
  }

  useEffect(() => {
    if (selectedItemId) {
      // Find the item with the matching ID
      const selectedItem = foodItems.filter(item => item._id === selectedItemId)[0];
      setSelectedItem(selectedItem);
    }
  }, [selectedItemId, foodItems]);

  const handleCloseModel=()=>{
    console.log('close modal');
    setSelectedItem(null);
  }
  const handelReservationConfirmation=(e)=>{
    e.preventDefault();
    setReservationConfirmed(true);
    console.log('confirmationDetails',confirmationDetails);
  }
  function changehandler(event){
      const{name,value, checked, type}=event.target;
      setconfirmationDetails((prev)=>({...prev,[name]:value}))
      console.log('confirmationDetails',confirmationDetails);
  }
  const handleCanselreservation=()=>{
    alert('Alert ! your order is going to cancel')
    setReservationConfirmed(false);
    console.log('cancel');

  }


  const navigate=useNavigate();

  useEffect (() => {
  //  const styleobject= document.getElementById('navbar_container');
  //  styleobject.style.display='block';
  //  const authenticationToken=sessionStorage.getItem('auth-token');
      const authenticationToken=localStorage.getItem('auth-token');
     

  if (!authenticationToken) {
    navigate('/signup')
  }
    
  })
  function redirectToList(){
    navigate('/resturantlist')
  }
  return (
      <div>
        <h1>LIST OF RESTAURANT</h1>
        <button className='button1' onClick={()=>redirectToList()}>Click To See List </button>
      <h1>DASHBOARD</h1>
      <div className="palaces_container">
        {
        foodItems.map((place)=>(
          //console.log('place',place);
           <div key={place.id} className='place-card'>
            <img src={place.picture} alt={place.name} />
            <h3>Food:{place.name}</h3>
            <p>Discription:{place.description}</p>
            <p>Quantity:{place.quantity}</p>
            <p>Price:{place.price}</p>
            {
              available? (
                <button onClick={()=>handelReservation(place._id)}>Book Now</button>
              ):(
              <button>Fully Booked</button>
             ) }
          </div>

        ))
        }
        </div>
        {selectedItem && (
          <div className="modal">
            <div className="modal-content">
              <span className='close' onClick={handleCloseModel}>&times;</span>             
              {
                !reservationConfirmed ?
                 (<div className="showform">
                  <form onSubmit={handelReservationConfirmation} className='formdata'>
                     <h2>Selected Item</h2>
                     <img src={selectedItem.picture} alt={selectedItem.name} />
                     <p>Name: {selectedItem.name}</p>
                     <p>Price: {selectedItem.price}</p>
                     <p>Quantity: {selectedItem.quantity}</p>
                    
                    

                    
                  <button className='btnReservation' >Proceed to Confirmation</button>
                  </form>
                 </div>
                   ):(
                     <div className="showcard">
                     <p>You have sucessfully placed your order to cancel press cancel {confirmationDetails.reservationUserName}</p>
                     <button className='btnReservation' onClick={handleCanselreservation}>Cancel ORDER</button>
                     </div>
                   )
              }
            </div>
          </div>
        )}
     </div>
  )
}


