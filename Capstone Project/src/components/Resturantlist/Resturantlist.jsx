import React, { useEffect, useState } from 'react'
import './Resturantlist.css';
import {  useNavigate } from 'react-router-dom'
import {api_uri} from '../../config';

const Resturantlist = () => {
   
  const [list, setList] = useState([]);

  useEffect(() => {
    // Fetch food items from backend when component mounts
    fetch(`${api_uri}/api/authr/registerrst`)
      .then(response => response.json())
      .then(data => setList(data))
      .catch(error => console.error('Error fetching reaturant list:', error));
  }, []);

  
  const navigate=useNavigate();

  useEffect (() => {
        const authenticationToken=localStorage.getItem('auth-token');
        if (!authenticationToken) {
    navigate('/signup')
  }
    
  })

  return (
    <div>
    <h1>Restaurant List</h1>
    <div className="palaces_container">
      {
      list.map((place)=>(
        //console.log('place',place);
         <div key={place.id} className='place-card'>
          <h3>Resturant Name:{place.name}</h3>
          <p>Address:{place.address}</p>
          <p>Closing Time:{place.time}</p>
    </div>

      ))
      }
      </div>
      </div>
  )
}

export default Resturantlist