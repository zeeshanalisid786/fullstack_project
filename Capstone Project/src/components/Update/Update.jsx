import React, { useState, useEffect } from 'react';
import { useNavigate, useParams, } from 'react-router-dom'
import './Update.css';
import {api_uri} from '../../config';
const Update = () => 
  {
    const {id}=useParams()
    const [name, setName] = useState();
    const [quantity, setQuantity]=useState();
    const [price, setPrice] = useState();
    const [description, setDescription] = useState();
    const [picture, setPicture] = useState();
    const navigate= useNavigate()

    useEffect(() => {
      // Fetch food items from backend when component mounts
      fetch(`${api_uri}/api/foodItems/handleAddFoodItem`+id)
        .then(response => response.json())
        .then(data => setFoodItems(data))
        .catch(error => console.error('Error fetching food items:', error));
    }, []);

    const Update = async (id) => {
      e.preventDefalt();
        try {
          const response = await fetch(`${api_uri}/api/foodItems/${id}`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              name:name,
              price:price,
              quantity:quantity,
              description:description,
              picture:picture
            })
            
          });
      
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          const json=await response.json();
          setFoodItems(json);
          // Refresh the list of food items after update
          //window.location.reload();
        } catch (error) {
          console.error('Error updating food item:', error);
        }
      };


  return (
    
          
        <div className="input-container">
          <h1>Update Data</h1>
        <form onSubmit={Update} >
        <div className="container">
          <input
            type="text"
            placeholder="Item Name"
            name='name'
            value={name}
            onChange={(e)=>setName(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Quantity"
            name='quantity'
            value={quantity}
            onChange={(e)=>setQuantity(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Price"
            name='price'
            value={price}
            onChange={(e)=>setPrice(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Description"
            name='description'
            value={description}
            onChange={(e)=>setDescription(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Picture URL"
            name='picture'
            value={picture}
            onChange={(e)=>setPicture(e.target.value)}
          />
          <button type='submit'>Update</button>
        </div>
        
        </form >
        </div>
    
  )
}

export default Update