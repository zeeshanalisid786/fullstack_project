import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import './Navbar.css'
const Navbar = () => {
   const[isLoggedin, setIsLoggedin]=useState(false);
    const[username, setUsername]=useState();

    useEffect(() => {
      const authtokensession=localStorage.getItem('auth-token');
      const nameFromSession=localStorage.getItem('name');
      // const authtokensession=sessionStorage.getItem('auth-token');
      // const nameFromSession=sessionStorage.getItem('name');
      if (authtokensession) {
        //console.log(authtokensession);
        setIsLoggedin(true);
        setUsername(nameFromSession);
      }
    })
    const handlelogout =() => {

      //  sessionStorage.removeItem('auth-token');
      //  sessionStorage.removeItem('name');
      //   setIsLoggedin(false);
      //   window.location.reload();

       localStorage.removeItem('auth-token');
       localStorage.removeItem('name');
        setIsLoggedin(false);
        window.location.reload();
    }
  return (
    <>
    <div id='navbar_container'>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
   <Link to='/' class="navbar-brand">FOO Go</Link>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <Link class="nav-link" to="/">Home <span class="sr-only"></span></Link>
      </li>
      <li class="nav-item">
        <Link class="nav-link" to="/customer">Resturant</Link>
      </li>
      </ul>  

      <form class="d-flex">
      <input class="form-control1 mr-2" type="search" placeholder="Search" aria-label="Search"/>
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>

    <ul class="navbar-nav mr-auto mb2 mb-lg-0">
      <li class="nav-item active">
        <Link class="nav-link" to="#">Recommended<span class="sr-only"></span></Link>
      </li>
      {isLoggedin?(
        <>
          <li class="nav-item">
            <Link class="nav-iteam " to='#'> Welcome<br/> {username}</Link>
          </li>
          <li class="nav-item">
            <Link class="nav-link active "><button className='nav-link active' onClick={handlelogout}>LogOut</button></Link>
          </li>
        </>
      ):(
        <>
        <li class="nav-item">
        <Link class="nav-iteam" to="#">LogIn</Link>
      </li>
        </>
      )}
      </ul>     
  </div>
</nav>
</div>
    </>
  )
}

export default Navbar