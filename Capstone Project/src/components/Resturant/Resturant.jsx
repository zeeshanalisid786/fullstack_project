import React, { useState, useEffect } from 'react';
import { Link,useNavigate } from "react-router-dom";
import './Resturant.css';
import {api_uri} from '../../config';


const Resturant = () => {
    const [foodItems, setFoodItems] = useState([]);
    const [name, setName] = useState();
    const [quantity, setQuantity]=useState();
    const [price, setPrice] = useState();
    const [description, setDescription] = useState();
    const [picture, setPicture] = useState();
   
      useEffect(() => {
    // Fetch food items from backend when component mounts
    fetch(`${api_uri}/api/foodItems/handleAddFoodItem`)
      .then(response => response.json())
      .then(data => setFoodItems(data))
      .catch(error => console.error('Error fetching food items:', error));
  }, []);
   
  const handleAddFoodItem =async (e) => {
    e.preventDefault();
    // Send POST request to add new food item\
    try{
    const response= await fetch(`${api_uri}/api/foodItems/handleAddFoodItem`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name:name,
        price:price,
        quantity:quantity,
        description:description,
        picture:picture
      })
    });
    const json=await response.json();
    setFoodItems(json);
    alert("FOOD ADDED SUCCESSFULLY");
    window.location.reload();
    //console.log(foodItems);
    //console.log('jason',json);
    }catch(error){
  console.error('Error adding food item:', error);
  }  
};


const handleDeleteFoodItem =async (id) => {
  // Send DELETE request to delete food item
 const response=await fetch(`${api_uri}/api/foodItems/${id}`, {
    method: 'DELETE'
  })
    .then(() => { 
      alert("Do you really wish to delete this food from your menue");
     // console.log(id);
      setFoodItems(foodItems.filter(item => item._id !== id));
    })
    .catch(error => console.error('Error deleting food item:', error));
};

// const handleUpdate = async (id) => {
//   try {
//     const response = await fetch(`${api_uri}/api/foodItems/${id}`, {
//       method: 'PUT',
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         name:name,
//         price:price,
//         quantity:quantity,
//         description:description,
//         picture:picture
//       })
      
//     });

//     if (!response.ok) {
//       throw new Error('Network response was not ok');
//     }
//     const json=await response.json();
//     setFoodItems(json);
//     // Refresh the list of food items after update
//     //window.location.reload();
//   } catch (error) {
//     console.error('Error updating food item:', error);
//   }
// };

const navigate=useNavigate();

  useEffect (() => {
  //  const styleobject= document.getElementById('navbar_container');
  //  styleobject.style.display='block';
  //  const authenticationToken=sessionStorage.getItem('auth-token');
      const authenticationToken=localStorage.getItem('auth-token');
     

  if (!authenticationToken) {
    navigate('/resturantSignup')
  }
    
  })

  return (
    <div>
      <h1>Restaurant Dashboard</h1>
      <div >
        <h1>Add Food Item</h1>
        <div className="input-container">
        <form onSubmit={handleAddFoodItem} >
        <div className="container">
          <input
            type="text"
            placeholder="Item Name"
            name='name'
            value={name}
            onChange={(e)=>setName(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Quantity"
            name='quantity'
            value={quantity}
            onChange={(e)=>setQuantity(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Price"
            name='price'
            value={price}
            onChange={(e)=>setPrice(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Description"
            name='description'
            value={description}
            onChange={(e)=>setDescription(e.target.value)}
          />
        </div>
        <div className="container">
          <input
            type="text"
            placeholder="Picture URL"
            name='picture'
            value={picture}
            onChange={(e)=>setPicture(e.target.value)}
          />
          <button type='submit'>Add</button>
        </div>
        
        </form >
        </div>
      </div>
      <h1>Food Items</h1>
    <div className="container">
        {
        foodItems.map(item => (
          <div className="card" key={item._id}>
            <img src={item.picture} alt={item.name} />
            <p>Name:{item.name}</p>
            <p>Quantity: {item.quantity}</p>
            <p>Price: {item.price}</p>
            <p>Description: {item.description}</p>
            <button onClick={() => handleDeleteFoodItem(item._id)}>Delete</button>
            <br></br>
            <Link to={`/update/${item.id}`} className='btn btn-success' > Update </Link>
          </div>
        ))}
      </div>
     
    </div>
  );
};


export default Resturant