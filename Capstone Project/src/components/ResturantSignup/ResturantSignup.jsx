import React, { useEffect,useState } from 'react'
import { Link,  useNavigate } from 'react-router-dom'
import {api_uri} from '../../config';
import "./ResturantSignup.css"

const ResturantSignup = () => {
    const[name,setName]=useState();
    const[email,setEmail]=useState();
    const[address,setAddress]=useState();
    const[time,setTime]=useState();
    const[password,setPassword]=useState();
    const [showerr,setShowere]=useState();
    const [passError,setPassError]=useState();
    const navigate=useNavigate();   
    useEffect (() => {
        const styleobject= document.getElementById('navbar_container');
        styleobject.style.display='block';
    })



    const Registerrst=async(e)=>{
        e.preventDefault();
        //api call
        if (password.length<=8) {
           const message='Password should be of minimum 8 characters';
           setPassError(message)
        }
        else{
        const response= await fetch(`${api_uri}/api/authr/registerrst`,{
        method:'POST',
        headers:{
           'content-type':'application/json'
        },
        body:JSON.stringify({
           name:name,
           address :address,
           time:time,
           password:password,
           email:email,
        })
       })
       const json=await response.json();
    if (json.authtoken) {
        localStorage.setItem('auth-token',json.authtoken);
        localStorage.setItem('name',name);
        navigate('/resturant');
        window.location.reload();
    }
    if (json.error) {
        //showerr=json.error;
        setShowere(json.error);
        //console.log();

    }
}
    }
  return (
    <>
    <div className='signup_container'>
        <div className="signup_main">
            <div className="signupgrid">
                <h1>Restaurant Signup</h1>
            </div>
            <div className="signup_text">
                Already a member <Link to='/resturantlogin'>LogIn</Link>
            </div>
            <div className="signup_form">
                <form onSubmit={Registerrst}>
                    {/* name */}
                    <div className="mb-3">
                        <label htmlFor='name' className='form-label'>Enter Restaurant Name</label>
                        <input type='text' id='name' className='form-control' placeholder='Enter Name' value={name} onChange={(e) =>setName(e.target.value)}/>
                    </div>
                    {/* Address */}
                    <div className="mb-3">
                        <label htmlFor='address' className='form-label'>Enter Address</label>
                        <input type='text' id='address' className='form-control' placeholder='Enter address' value={address} onChange={(e) =>setAddress(e.target.value)}/>
                        </div>
                        {/* Time */}
                    <div className="mb-3">
                        <label htmlFor='time ' className='form-label'>Enter Closing Time</label>
                        <input type='text' id='time' className='form-control' placeholder='Enter time' value={time} onChange={(e) =>setTime(e.target.value)}/>
                        </div>

                    {/* Email */}
                    <div className="mb-3">
                        <label htmlFor='email' className='form-label'>Enter Email</label>
                        <input type='text' id='email' className='form-control' placeholder='Enter Email' value={email} onChange={(e) =>setEmail(e.target.value)}/>
                        {showerr && <div style={{color:'blue',fontSize:'20px',fontWeight:'bold'}} >{showerr}</div>}
                        
                        </div>
                        
                        {/* Password */}
                    <div className="mb-3">
                        <label htmlFor='password' className='form-label'>Enter Password</label>
                        <input type='password' id='password' className='form-control' placeholder='Enter Password' value={password} onChange={(e) =>setPassword(e.target.value)}/>
                        {passError && <div style={{color:'blue',fontWeight:'bold',fontSize:'20px'}} >{passError}</div>}
                    </div>
                    <button type='submit' className='btn_signp'>Submit</button>
                </form>
            </div>
        </div>
    </div>
    </>
  )
}

export default ResturantSignup