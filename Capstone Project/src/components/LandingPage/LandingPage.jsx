import React, { useEffect } from 'react'
import './LandingPage.css'
import { redirect, useNavigate } from 'react-router-dom'
const LandingPage = () => {
  const navigate=useNavigate();
  useEffect(() => {
    const navinfo=document.getElementById('navbar_container');
    //console.log(navinfo);
    navinfo.style.display='none';
  })
    
  function redirectToCustomer(){
    navigate('/signup')
  }
  function redirectToResturant(){
    navigate('/resturantSignup')
  }
  return (
    <>
     <div className="landing_container">
      <div className='landing_page'>
        <img src="/fogo.png" alt="break"/>
            <h1>WELCOME TO FOOGO </h1>
      <div className='landing_btn'>
            <button className='customer_btn' onClick={()=>redirectToCustomer()}>Customer LogIn</button>
            <button className='resturant_btn' onClick={()=>redirectToResturant()}>Resturant LogIn</button>
     </div>
     </div>
     </div>
    </>
  )
}

export default LandingPage