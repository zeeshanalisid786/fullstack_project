import {BrowserRouter, Routes,Route} from 'react-router-dom'
import './App.css'
import LandingPage from './components/LandingPage/LandingPage'
import Customer from './components/Customer/Customer'
import Resturant from './components/Resturant/Resturant'
import Login from './components/Login/Login'
import Signup from './components/Signup/Signup'
import Navbar from './components/Navbar/Navbar'
import Update from './components/Update/Update'
import ResturantSignup from './components/ResturantSignup/ResturantSignup'
import Resturantlogin from './components/Resturantlogin/ResturantIogin'
import Resturantlist from './components/Resturantlist/Resturantlist'
function App() {
  

  return (
 <>
   <BrowserRouter>
   <Navbar/>
   <Routes>
    <Route path='/' element={<LandingPage/>} />
    <Route path='/customer' element={<Customer/>} />
    <Route path='/resturant' element={<Resturant/>} />
    <Route path='/signup' element={<Signup/>} />
    <Route path='/login' element={<Login/>} />
    <Route path='/update' element={<Update/>} />
    <Route path='/resturantSignup' element={<ResturantSignup/>} />
    <Route path='/resturantlogin' element={<Resturantlogin/>} />
    <Route path='/resturantlist' element={<Resturantlist/>} />
   </Routes>
   </BrowserRouter>
  </>
  )
}

export default App
