const express = require('express');
const app=express();
const jwt=require('jsonwebtoken');
const router = express.Router();
const FoodItem = require('../models/FoodItem');
const {body, validationResul}=require('express-validator');
const dotenv=require('dotenv');
dotenv.config();
const JWT_SECRET=process.env.JWT_SECRET;

// Get all food items
router.get('/handleAddFoodItem', async (req, res) => {
  //console.log('fooditems',FoodItem);
  try {
    const foodItems = await FoodItem.find();
    res.json(foodItems);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create a food item
router.post('/handleAddFoodItem', async (req, res) => {
  const fooditems = new FoodItem(req.body);
  try {
    const newFoodItem =await fooditems.save({
      name:req.body.name,
      price:req.body.price,
      quantity:req.body.quantity,
      description:req.body.description,
      picture:req.body.picture
    })
    
    const payload={
      FoodItem:{
               id:newFoodItem
      }
    }
    const data=jwt.sign(payload,JWT_SECRET);
    res.json({data})
  } catch (e) {
    console.log(e);
  }
});

// Delete a food item
router.delete('/:id', async (req, res) => {
  const id = req.params.id;

  try {
    const deletedItem = await FoodItem.findByIdAndDelete(id);

    if (!deletedItem) {
      return res.status(404).json({ message: 'Food item not found' });
    }

    res.status(200).json({ message: 'Food item deleted successfully' });
  } catch (error) {
    console.error('Error deleting food item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

// Update a food item by ID
router.put('/:id', async (req, res) => {
  const id = req.params.id;

  try {
    const updatedItem = await FoodItem.findByIdAndUpdate(id, req.body, { new: true });

    if (!updatedItem) {
      return res.status(404).json({ message: 'Food item not found' });
    }

    res.status(200).json(updatedItem);
  } catch (error) {
    console.error('Error updating food item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});



module.exports = router;
