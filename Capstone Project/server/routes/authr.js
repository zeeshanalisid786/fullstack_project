const express=require('express');
const app=express();
const bcryptjs=require('bcryptjs');
const jwt=require('jsonwebtoken');
const resturantDetails=require('../models/resturant');
const router=express.Router();
const {body, validationResul}=require('express-validator');
const dotenv=require('dotenv');
dotenv.config();
const JWT_SECRET=process.env.JWT_SECRET;
//console.log('JWT_SECRET',JWT_SECRET);

router.post('/registerrst',async(req, res)=>{
    
    try{
        const existingEmail=await resturantDetails.findOne({email:req.body.email});
        if (existingEmail) {
            return res.status(400).json({error:'Email id already exist'})
        }
        const salt=await bcryptjs.genSalt(10);
        const hashPassword=await bcryptjs.hash(req.body.password,salt);
        const newResturant=await resturantDetails.create({
            email:req.body.email,
            password:hashPassword,
            name:req.body.name,
            time:req.body.time,
            address:req.body.address,
        })
        //console.log(newResturant);
        const payload={
            resturant:{
                id:newResturant.id
            }
        }
      const authtoken=jwt.sign(payload,JWT_SECRET);
     res.json({authtoken})
    }

    catch(e){
        console.log(e);
    }

})
//get all resturant
router.get('/registerrst', async (req, res) => {
    //console.log('fooditems',FoodItem);
    try {
      const resturant = await resturantDetails.find();
      res.json(resturant);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  });

router.post('/resturantlogin',async(req , res)=>{
    try {
        console.log('req',req.body.email);
        const theUser=await resturantDetails.findOne({email:req.body.email});//find the email from DB
        console.log(theUser);
        if(theUser){
            let payload={
                user:{
                    id:theUser.id
                }
            };
            const expiresIn=3600;
            const authtoken=jwt.sign(payload,JWT_SECRET,{expiresIn});

            return res.status(200).json({authtoken,userName:theUser.name})
           
        }
    } 
    catch (e) {
                console.log(e);        
    }
})

module.exports= router;