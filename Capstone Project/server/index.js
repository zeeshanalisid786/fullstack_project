const express=require('express');
const app=express();
const port=3500;
const cors=require('cors');
const foogoconnection=require('./db');
const foodItemRouter=require('./routes/foodItems');
app.use(express.json());
app.use(cors());
foogoconnection();


app.use('/api/auth',require('./routes/auth'));
app.use('/api/fooditems',require('./routes/foodItems'));
app.use('/api/authr',require('./routes/authr'));
app.get('/',(req, res) => {
    res.send('hello world');
})



app.listen(port,()=>{
    console.log('server started');
})