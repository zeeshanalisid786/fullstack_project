const mongoose=require('mongoose');
const mongoPath='mongodb://127.0.0.1:27017/foogodatabase'
const foogoconnection=async() =>{
    try{
        await mongoose.connect(mongoPath,{dbName:'foogodatabase'});
        console.log('database has been connected');
    }
    catch(e){
        console.log(e);
    }
}
module.exports=foogoconnection;