const mongoose=require('mongoose');

const resturantSchema=new mongoose.Schema({
name:{
    type:String,
    required:true,
},
address:{
    type:String,
    required:true,
},
time:{
    type:Number,
    required:true,
},
email:{
    type:String,
    required:true,
},
password:{
    type:String,
    required:true,
}

})

const resturantDetails=new mongoose.model('resturant',resturantSchema);

module.exports= resturantDetails;